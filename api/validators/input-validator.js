class InputValidator {

  static validateName(name) {
    if (name) {
      return Promise.resolve(name)
    } else {
      return Promise.reject({
        statusCode: 400,
        message: 'Name must be a valid non empty string.'
      });
    }
  }

  static validateEmail(email) {
    let emailRegexp = /^(?=.{1,254}$)(?=.{1,64}@)[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+(\.[-!#$%&'*+/0-9=?A-Z^_`a-z{|}~]+)*@[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?(\.[A-Za-z0-9]([A-Za-z0-9-]{0,61}[A-Za-z0-9])?)*$/;
    const validateEmail = emailRegexp.test(email);
    if (validateEmail == true) {
      return Promise.resolve(email)
    } else {
      return Promise.reject({
        statusCode: 400,
        message: `${email} is not a valid email`
      });
    }
  }

  static validatePassword(password) {
    if(password){
      return Promise.resolve(password)
    } else{
      return Promise.reject({
        statusCode: 400,
        message: 'password must be a valid non empty string '
      });
    }

  }
}

module.exports.InputValidator = InputValidator;
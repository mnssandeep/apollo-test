const { UserService } = require('../services/user-service');

class ProfileController {

  constructor(db) {
    this.userService = new UserService(db);
    this.getUserProfile = this.getUserProfile.bind(this);
  }

  getUserProfile(req, res, next) {
    const token = req.headers.authorization.split('Bearer')[1];
    this.userService.getUserProfileByToken(token).then(user_details=>{
      res.json(user_details)
    }).catch(error=> next(error));


  }
}

module.exports.ProfileController = ProfileController;

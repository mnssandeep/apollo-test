const { UserService } = require('../services/user-service');
const { InputValidator } = require('../validators/input-validator');

class AuthenticationController {
  constructor(db) {
    this.userService = new UserService(db);
    this.registerUser = this.registerUser.bind(this);
    this.logIn = this.logIn.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  // Validate Email
  // Validate Password
  // Save User
  registerUser(req, res, next) {
    InputValidator.validateEmail(req.body.email).then(valid_email =>{
      InputValidator.validateName(req.body.name).then(valid_name =>{
       InputValidator.validatePassword(req.body.password).then(valid_password=>{
        this.userService
        .registerUser(valid_name,valid_email,valid_password)
        .then(userDetails=>{
          res.json(userDetails)
        }).catch(error=> next(error));
       }).catch(error=> next(error));
     }).catch(error=> next(error));
    }).catch(error=> next(error));
 
    
    
  
     
    

  }

  logIn(req, res, next) {
    this.userService.logIn(req.body.email,req.body.password).then(logindetails=>{
      res.json(logindetails)
    }).catch(error=> next(error))
  }

    logOut(req, res, next) {
    console.log("sevice");
    const token = req.headers.authorization.split('Bearer')[1];
    this.userService.logOut(token).then(logindetails=>{
        res.json(logindetails)
      }).catch(error=> next(error))
    }
  }

module.exports.AuthenticationController = AuthenticationController;

const uuid = require('uuid');
const crypto = require('crypto');
const q = require('q')

function hash(str) {
  const hmac = crypto.createHmac('sha256', process.env.HASH_SECRET || 'test-secret');
  hmac.update(str);
  return hmac.digest('hex');
}



function createToken() {
  return 'token.' + uuid.v4().split('-').join('');
}

class UserService {
  constructor(db) {
    this.db = db;
    this.getUserProfileByToken = this.getUserProfileByToken.bind(this);
    this.registerUser = this.registerUser.bind(this);
    this.logIn = this.logIn.bind(this);
    this.logOut = this.logOut.bind(this);
  }

  /**
   * Registers a user and returns it's token
   * @param {String} name
   * @param {String} email
   * @param {String} password
   * @return {Promise} resolves to user's token or rejects with Error that has statusCodes
   */
  registerUser(name, email, password) {
    let deferred = q.defer();
    let token = createToken()
    password = hash(password);
    const collection = this.db.collection('users');
    collection.insert({
      name,
      email,
      password,
      token
    }, function (error, user_details) {
      console.log(user_details.ops);
      if (error) {
        deferred.reject(new Error(error))
      } else {
        deferred.resolve({
          "_id": user_details.ops[0]._id,
          token: user_details.ops[0].token
        })
      }
    })

    return deferred.promise;


  }

  /**
   * Gets a user profile by token
   * @param {String} token
   * @return {Promise} that resolves to object with email and name of user or rejects with error
   */
  getUserProfileByToken(token) {
    let collection = this.db.collection('users');
    let deferred = q.defer();
    collection.findOne({
      token: token
    }, function (error, users) {
      if (error) {
        deferred.reject(new Error(error))
      } else {
        deferred.resolve(users)
      }
    })

    return deferred.promise
  }

  /**
   * Log in a user to get his token
   * @param {String} email
   * @param {String} password
   * @return {Promise} resolves to token or rejects to error
   */
  logIn(email, password) {
    let deferred = q.defer();
    const collection = this.db.collection('users');
    collection.findOne({
      email: email
    }, function (error, users) {
      if (hash(password) == users.password) {
        deferred.resolve({
          "_id": users._id,
          token: users.token
        })
      } else {
        deferred.reject(new Error(error))
      }
    })
    return deferred.promise
  }

  logOut(token) {
    let deferred = q.defer();
    const collection = this.db.collection('users');
    collection.findOne({
      token: token
    }, function (error, users) {
      if (users) {
        deferred.resolve(users)
      } else {
        deferred.reject(error)
      }

    })
    return deferred.promise
  }

}

module.exports.UserService = UserService;